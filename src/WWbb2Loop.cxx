//wwbb2l2x
#include "wwbb2l2x/WWbb2Loop.h"
#include "wwbb2l2x/utility/misc_helpers.h"
#include "wwbb2l2x/MT2_ROOT.h"

//xAOD
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "PathResolver/PathResolver.h"
#include "xAODMetaData/FileMetaData.h"
#include "xAODBase/IParticleHelpers.h" // setOriginalObjectLink

//std/stl
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <fstream>
using namespace std;

//ROOT
#include "TFile.h"
#include "TLorentzVector.h"
#include "TChainElement.h"

const static SG::AuxElement::ConstAccessor<char> acc_bad("bad");
const static SG::AuxElement::ConstAccessor<char> acc_cosmic("cosmic");
const static SG::AuxElement::ConstAccessor<char> acc_baseline("baseline");
const static SG::AuxElement::ConstAccessor<char> acc_signal("signal");
const static SG::AuxElement::ConstAccessor<char> acc_passOR("passOR");
const static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");

const static SG::AuxElement::Decorator<int> dec_charge("MyCharge");
const static SG::AuxElement::ConstAccessor<int> acc_charge("MyCharge");

const static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");
const static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");


#define ADDCUT(name) \
    do { \
        _cut_names.push_back(#name); \
        _raw_cut_map[#name] = 0; \
    } while(0);

#define ADDBRANCH(name) \
    do { \
        _rtree->Branch(#name, &b_##name); \
    } while(0);

namespace wwbb {
//____________________________________________________________________________
WWbb2Loop::WWbb2Loop() :
    m_verbose(false),
    _do_flav("ee"),
    _do_muon_jet(false),
    m_is_mc(false),
    m_do_uncerts(false),
    m_print_weights(false),
    m_suffix(""),
    m_do_slim(false),
    m_nEventsProcessed(0),
    m_sumOfWeights(0),
    m_sumOfWeightsSquared(0),
    n_events_processed(0),
    _susyObj(nullptr),
    _grl_tool(""),
    _electrons(0),
    _electrons_aux(0),
    _muons(0),
    _muons_aux(0),
    _jets(0),
    _jets_aux(0),
    _met(0),
    _met_aux(0),
    _photons(0),
    _photons_aux(0),
    _taus(0),
    _taus_aux(0),
    _rfile(nullptr),
    _rtree(nullptr)
{
    cout << __hhfunc__ << endl;
    m_event = new xAOD::TEvent(xAOD::TEvent::kClassAccess);
    m_store = new xAOD::TStore();

    b_weight_names = new std::vector<std::string>();
    b_weights = new std::vector<float>();
    //b_weight_deltas = new std::vector<float>();
}
//____________________________________________________________________________
void WWbb2Loop::Init(TTree* tree)
{
    initialize_cutflow();

    if(do_uncerts())
        get_sumw();

    cout << __hhfunc__ << endl;
    event()->readFrom(tree);
    event()->getEntry(0); // this is needed so that we can grab the EventInfo object

    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }

    m_is_mc = ei->eventType( xAOD::EventInfo::IS_SIMULATION );

    string simflavor_from_metadata = "";
    string amitag_from_metadata = "";
    if(!get_metadata_info(tree, m_is_derivation, simflavor_from_metadata, amitag_from_metadata))
    {
        cout << __hhfunc__ << "    ERROR Failed to obtain info from MetaData tree" << endl;
        exit(1);
    }
    m_is_af2 = simflavor_from_metadata.find("ATLFASTII") != std::string::npos;
    m_is_derivation = true;
    if(amitag_from_metadata.find("r9364") != std::string::npos) m_mctype = "mc16a";
    else if(amitag_from_metadata.find("r9781") != std::string::npos) m_mctype = "mc16c";
    else if(amitag_from_metadata.find("r10201") != std::string::npos) m_mctype = "mc16d";
    else if(amitag_from_metadata.find("r10724") != std::string::npos) m_mctype = "mc16e";
    else {
        cout << __hhfunc__ << "    ERROR Failed to determine MC campaign!" << endl;
        exit(1);
    }
    cout << __hhfunc__ << "    Treating sample as " << (m_is_af2 ? "AFII" : "FullSim") << endl;
    cout << __hhfunc__ << "    Treating MC sample as MC campaign " << m_mctype << endl;

    if(!initialize_susytools()) {
        cout << __hhfunc__ << "    ERROR Failed to initialize SUSYTools. Exitting." << endl;
        exit(1);
    }
    if(!initialize_grl()) {
        cout << __hhfunc__ << "   ERROR Failed to initialize the GRL tool. Exitting." << endl;
        exit(1);
    }

    if(muon_jet())
    {
        initialize_muon_jet_outputs();
    }

    if(do_uncerts())
    {

        if(!initialize_truth_weight_tool())
        {
            cout << __hhfunc__ << "   ERROR Failed to initialize PMGTruthWeightTool" << endl;
            exit(1);
        }
        initialize_outputs();
        if(!initialize_lwtnn())
        {
            cout << __hhfunc__ << "   ERROR Failed to initialize LWTNN" << endl;
            exit(1);
        }
    }

}
//____________________________________________________________________________
void WWbb2Loop::get_sumw()
{
    // get the sumw of all of the files
    TObjArray* chain_files = input_chain()->GetListOfFiles();
    TIter next(chain_files);
    TChainElement* ch_file = 0;
    int file_counter = 0;
    while (( ch_file = (TChainElement*)next() )) {
        cout << "XaodAnalsysis::get_sumw    CutBookkeeper info for: " << ch_file->GetTitle() << endl;
        TFile* f = TFile::Open(ch_file->GetTitle());
        m_event->readFrom(f);
        m_event->getEntry(0);
        if(!collect_cutbooks(*m_event, file_counter)) exit(1);
        f->Close();
        f->Delete();
        file_counter++;
    } // while
    cout << __hhfunc__ << "    ------------------------------------------------------------------" << endl;
    cout << __hhfunc__ << "     CutBookkeeper totals: " << endl;
    cout << __hhfunc__ << "      > # events processed   : " << m_nEventsProcessed << endl;
    cout << __hhfunc__ << "      > sum of weights       : " << m_sumOfWeights << endl;
    cout << __hhfunc__ << "      > sum of weights^2     : " << m_sumOfWeightsSquared << endl;
    cout << __hhfunc__ << "    ------------------------------------------------------------------" << endl;

}
bool WWbb2Loop::collect_cutbooks(xAOD::TEvent& event, int file_counter)
{
    bool ok = true;
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    ok = event.retrieveMetaInput(completeCBC, "CutBookkeepers");
    if(!ok) {
        cout << __hhfunc__ << "    ERROR Failed to get CBK metdata for file "
                << file_counter << " in input chain, exiting" << endl;
        exit(1);
    }

    const xAOD::CutBookkeeper* allEventsCBK = 0;
    int maxcycle = -1;
    for(auto cbk : *completeCBC) {
        if(cbk->name() == "AllExecutedEvents" && cbk->inputStream()=="StreamAOD" && cbk->cycle() > maxcycle) {
            maxcycle = cbk->cycle();
            allEventsCBK = cbk;
        } // fi
    } // cbk
    uint64_t nevents_ = -1;
    double sumw_ = -1.0;
    double sumw2_ = -1.0;
    if(allEventsCBK) {
        nevents_ = allEventsCBK->nAcceptedEvents();
        sumw_ = allEventsCBK->sumOfEventWeights();
        sumw2_ = allEventsCBK->sumOfEventWeightsSquared();
    }
    else {
        cout << __hhfunc__ << "     ERROR \"AllExecutedEvents\" branch "
                << "not found in CBK metadata for file " << file_counter << " in input chain, exiting" << endl;
        ok = false;
    }

    m_nEventsProcessed += nevents_;
    m_sumOfWeights += sumw_;
    m_sumOfWeightsSquared += sumw2_;

    if(verbose()) {
        cout << __hhfunc__ << "     > file[" << file_counter << "] # accepted events: " << m_nEventsProcessed
            << "  sumw: " << m_sumOfWeights << "  sumw2: " << m_sumOfWeightsSquared << endl;
    }

    return ok;

}
//____________________________________________________________________________
bool WWbb2Loop::initialize_lwtnn()
{
    string nn_loc = "wwbb2l2x/lwtnn_NN_jan15_mt2bb_3x_0p5_250node_adam.json";
    string nn_file = PathResolverFindCalibFile(nn_loc);
    if(nn_file == "")
    {
        cout << __hhfunc__ << "   ERROR Failed to find NN location (expected location: " << nn_loc << endl;
        return false;
    }
    else
    {
        cout << __hhfunc__ << "    Found NN: " << nn_file << endl;
    }

    std::ifstream input_nn_file(nn_file);
    std::string output_layer_name = "OutputLayer";
    auto config = lwt::parse_json_graph(input_nn_file);
    _lwt = new lwt::LightweightGraph(config, output_layer_name);

    return true;
}
//____________________________________________________________________________
TDirectory* WWbb2Loop::get_directory_from_chain(TTree* tree)
{
    TDirectory* dir = nullptr;
    if(tree) {
        dir = tree->GetDirectory(); // file?
        if(dir) {
            if(verbose()) cout << __hhfunc__ << "    Got the directory from the input tree: "
                        << dir->GetName() << endl;
        }
        else {
            if(verbose()) cout << __hhfunc__ << "     Trying to get the directory from a TChain" << endl;
            if(TChain* c = dynamic_cast<TChain*>(tree)) {
                if(TChainElement* ce = static_cast<TChainElement*>(c->GetListOfFiles()->First())) {
                    TFile* first_file = TFile::Open(ce->GetTitle());
                    dir = static_cast<TDirectory*>(first_file);
                }
            }
        }
    }
    if(verbose()) {
        cout << __hhfunc__ << "    Got directory: " << (dir ? dir->GetName() : "NULL") << endl;
    }
    return dir;
}
//____________________________________________________________________________
bool WWbb2Loop::get_metadata_info(TTree* tree, bool & is_derivation, string& simflavor,
        string & amitag)
{

    TTree* metadata = nullptr;
    if(TDirectory* treedir = get_directory_from_chain(tree)) {
        if(treedir->Get("MetaData")) {
            metadata = dynamic_cast<TTree*>(treedir->Get("MetaData"));
        }
    }

    const xAOD::FileMetaData* fmd = nullptr;
    RETURN_CHECK( GetName(), m_event->retrieveMetaInput( fmd, "FileMetaData" ) );

    if(fmd) {
        fmd->value(xAOD::FileMetaData::amiTag, amitag);
        fmd->value(xAOD::FileMetaData::simFlavour, simflavor);
        std::transform(simflavor.begin(), simflavor.end(), simflavor.begin(), ::toupper);
    }
    else {
        cout << __hhfunc__ << "     WARNING Cannot get FileMetaData" << endl;
        amitag = "";
        simflavor = "";
    }

    if(metadata) {
        metadata->LoadTree(0);
        is_derivation = !metadata->GetBranch("StreamAOD");
    }
    else {
        cout << __hhfunc__ << "     WARNING Cannot get MetaData tree" << endl;
    }

    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::initialize_outputs()
{
    cout << __hhfunc__ << "   Initializing output ROOT file and TTree" << endl;
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }
    int dsid = ei->mcChannelNumber();

    stringstream outname;
    //outname << "ntup_";
    //outname << dsid;
    //if(suffix()!="")
    //{
    //    outname << "_" << suffix();
    //}
    outname << "wwbb2l2x.root";
    //outname << "_wwbb2l2x.root";

    _rfile = new TFile(outname.str().c_str(), "RECREATE");
    _rtree = new TTree("wwbb", "wwbb");

    // branches
    _rtree->Branch("weight_names", &b_weight_names);
    _rtree->Branch("weight_vec", &b_weights);
    //_rtree->Branch("weight_delta_vec", &b_weight_deltas);
    ADDBRANCH(nom_weight);
    ADDBRANCH(dsid);
    ADDBRANCH(mctype);
    ADDBRANCH(nom_sumw);
    ADDBRANCH(n_in_aod);
    ADDBRANCH(isEE);
    ADDBRANCH(isMM);
    ADDBRANCH(isEM);
    ADDBRANCH(isME);
    ADDBRANCH(isSF);
    ADDBRANCH(isDF);
    ADDBRANCH(l0_pt);
    ADDBRANCH(l1_pt);
    ADDBRANCH(l0_eta);
    ADDBRANCH(l1_eta);
    ADDBRANCH(l0_phi);
    ADDBRANCH(l1_phi);
    ADDBRANCH(mll);
    ADDBRANCH(pTll);
    ADDBRANCH(dphi_ll);
    ADDBRANCH(dRll);
    ADDBRANCH(met);
    ADDBRANCH(metPhi);
    ADDBRANCH(dphi_met_ll);
    ADDBRANCH(met_pTll);
    ADDBRANCH(nJets);
    ADDBRANCH(nBJets);
    ADDBRANCH(nSJets);
    ADDBRANCH(j0_pt);
    ADDBRANCH(j1_pt);
    ADDBRANCH(j2_pt);
    ADDBRANCH(bj0_pt);
    ADDBRANCH(bj1_pt);
    ADDBRANCH(bj2_pt);
    ADDBRANCH(sj0_pt);
    ADDBRANCH(sj1_pt);
    ADDBRANCH(sj2_pt);
    ADDBRANCH(j0_eta);
    ADDBRANCH(j1_eta);
    ADDBRANCH(j2_eta);
    ADDBRANCH(bj0_eta);
    ADDBRANCH(bj1_eta);
    ADDBRANCH(bj2_eta);
    ADDBRANCH(sj0_eta);
    ADDBRANCH(sj1_eta);
    ADDBRANCH(sj2_eta);
    ADDBRANCH(j0_phi);
    ADDBRANCH(j1_phi);
    ADDBRANCH(j2_phi);
    ADDBRANCH(bj0_phi);
    ADDBRANCH(bj1_phi);
    ADDBRANCH(bj2_phi);
    ADDBRANCH(sj0_phi);
    ADDBRANCH(sj1_phi);
    ADDBRANCH(sj2_phi);
    ADDBRANCH(mbb);
    ADDBRANCH(dphi_bb);
    ADDBRANCH(dRbb);
    ADDBRANCH(dR_ll_bb);
    ADDBRANCH(dphi_ll_bb);
    ADDBRANCH(dphi_j0_ll);
    ADDBRANCH(dphi_j0_l0);
    ADDBRANCH(dphi_bj0_ll);
    ADDBRANCH(dphi_bj0_l0);
    ADDBRANCH(mt2_ll);
    ADDBRANCH(mt2_bb);
    ADDBRANCH(mt2_llbb);
    ADDBRANCH(HT2);
    ADDBRANCH(HT2Ratio);
    ADDBRANCH(MT_1);
    ADDBRANCH(MT_1_scaled); 
    ADDBRANCH(NN_p_hh);
    ADDBRANCH(NN_p_top);
    ADDBRANCH(NN_p_zsf);
    ADDBRANCH(NN_p_ztt);
    ADDBRANCH(NN_d_hh);
    ADDBRANCH(NN_d_top);
    ADDBRANCH(NN_d_zsf);
    ADDBRANCH(NN_d_ztt);
    

    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::initialize_muon_jet_outputs()
{
    h_n_dR1 = new TH1F("h_n_dR1", "Number of Muons within #DeltaR < 1; #Delta R;", 10, 0, 10);
    h_n_dR04 = new TH1F("h_n_dR04", "Number of Muons within #DeltaR < 0.4; #Delta R;", 10, 0, 10);
    h_n_dR02 = new TH1F("h_n_dR02", "Number of Muons within #DeltaR < 0.2; #Delta R;", 10, 0, 10);

    h_mbb_uncorr = new TH1F("h_mbb_uncorr", "mbb;m_{bb} [GeV];;", 200, 0, 200);
    h_mbb_corr = new TH1F("h_mbb_corr", "mbb;m_{bb} [GeV];;", 200, 0, 200);

    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::initialize_truth_weight_tool()
{
    cout << __hhfunc__ << "    Initializing PMGTruthWeightTool" << endl;
    _weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
    RETURN_CHECK(GetName(), _weightTool.retrieve());

    auto weight_names = _weightTool->getWeightNames();
    b_weights->clear();
    //b_weight_deltas->clear();
    b_weight_names->clear();

    size_t widx = 0;
    size_t n_weights = weight_names.size();
    for(auto name : weight_names)
    {
        cout << __hhfunc__ << "   [" << widx << " / " << n_weights << "] Adding Weight: " << name << endl;
        b_weight_names->push_back(name);
    }
    return true;
}
//____________________________________________________________________________
void WWbb2Loop::pass_cut(string cutname)
{
    _raw_cut_map.at(cutname)++;
}
//____________________________________________________________________________
void WWbb2Loop::initialize_cutflow()
{
    ADDCUT(READIN)
    ADDCUT(GRL)
    ADDCUT(EVENTCLEANING)
    ADDCUT(GOODVERTEX)
    ADDCUT(BADMUON)
    ADDCUT(COSMICMUON)
    ADDCUT(JETCLEANING)
    ADDCUT(TRIGGER)
    ADDCUT(BASELINELEPTON)
    ADDCUT(SIGNALLEPTON)
    ADDCUT(OPPOSITECHARGE)
    ADDCUT(MLL20)
    ADDCUT(ZVETO)
    ADDCUT(BJETMULT)
    ADDCUT(HIGGSMBB)
}
//____________________________________________________________________________
vector<string> WWbb2Loop::ilumicalc_files()
{

//    vector<string> lumi_calcs;
//    bool lumi_calc_ok = true;
//
//    // assume mc16a
//    // 2015
//    string lumi_calc_2015_loc = "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root";
//    string lumi_calc_2015 = PathResolverFindCalibFile(lumi_calc_2015_loc); // FindDataFile
//    if(lumi_calc_2015=="") {
//        cout << "XaodAnalysis::prw_ilumicalc_files    ERROR PathResolver unable to find "
//            << "2015 ilumicalc file (=" << lumi_calc_2015_loc << ")" << endl;
//        lumi_calc_ok = false;
//    }
//    lumi_calcs.push_back(lumi_calc_2015);
//    
//    // 2016
//    string lumi_calc_2016_loc = "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root";
//    string lumi_calc_2016 = PathResolverFindCalibFile(lumi_calc_2016_loc);
//    if(lumi_calc_2016=="") {
//        cout << "XaodAnalysis::prw_ilumicalc_files    ERROR PathResolver unable to find "
//            << "2016 ilumicalc file (=" << lumi_calc_2016_loc << ")" << endl;
//        lumi_calc_ok = false;
//    }
//    lumi_calcs.push_back(lumi_calc_2016);
//    
//    // 2017
//    // dantrim September 25 2018 : actualMu files go under the "PRW configs" not the "LumiCalcFiles" but we still need to add the averageMu files
//     //see this talk for clarification: https://indico.cern.ch/event/712774/contributions/2928042/attachments/1614637/2565496/prw_mc16d.pdf
//    string lumi_calc_2017_loc = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"; // lumi-calc
//    string lumi_calc_2017 = PathResolverFindCalibFile(lumi_calc_2017_loc); // FindDataFile
//    if(lumi_calc_2017=="") {
//        cout << "XaodAnalysis::prw_ilumicalc_files    ERROR PathResolver unable to find "
//            << "2017 ilumicalc file (=" << lumi_calc_2017_loc << ")" << endl;
//        lumi_calc_ok = false;
//    }
//    lumi_calcs.push_back(lumi_calc_2017);
//
//    if(do_uncerts())
//    {
//        lumi_calcs.clear();
//        lumi_calcs.push_back(lumi_calc_2017);
//    }
//    else if(!muon_jet())
//    {
//        lumi_calcs.clear();
//        lumi_calcs.push_back(lumi_calc_2015);
//        lumi_calcs.push_back(lumi_calc_2016);
//    }
//    else
//    {
//        lumi_calcs.clear();
//        lumi_calcs.push_back(lumi_calc_2017);
//    }
//    
//    // 2018
//    // dantrim Sep 24 2018: full mc16e campaign is not ready yet, so I will not attempt perfect consideration of data18/mc16e
//    //string lumi_calc_2018_loc = "GoodRunsLists/data18_13TeV/20180906/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-001.root";
//    //string lumic_calc_2018 = PathResolverFindCalibFile(lumi_calc_2018_loc);
//    //if(lumi_calc_2018=="") {
//    //    cout << "XaodAnalysis::prw_ilumicalc_files    ERROR PathResolver unable to find "
//    //        << "2018 actualMu file (=" << lumi_calc_2018_loc << ")" << endl;
//    //    lumi_calc_ok = false;
//    //}
//    //lumi_calcs.push_back(lumi_calc_2018);
//
//    if(!lumi_calc_ok) exit(1);
//
//    return lumi_calcs;

    vector<string> lumi_calcs;
    
    bool lumi_calc_ok = true;
    
    // 2015
    string lumi_calc_2015_loc = "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root";
    string lumi_calc_2015 = PathResolverFindCalibFile(lumi_calc_2015_loc); // FindDataFile
    if(lumi_calc_2015=="") {
        cout << __hhfunc__ << "    ERROR PathResolver unable to find "
            << "2015 ilumicalc file (=" << lumi_calc_2015_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2015);
    
    // 2016
    string lumi_calc_2016_loc = "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root";
    string lumi_calc_2016 = PathResolverFindCalibFile(lumi_calc_2016_loc);
    if(lumi_calc_2016=="") {
        cout << __hhfunc__ << "    ERROR PathResolver unable to find "
            << "2016 ilumicalc file (=" << lumi_calc_2016_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2016);
    
    // 2017
    // dantrim September 25 2018 : actualMu files go under the "PRW configs" not the "LumiCalcFiles" but we still need to add the averageMu files
    // see this talk for clarification: https://indico.cern.ch/event/712774/contributions/2928042/attachments/1614637/2565496/prw_mc16d.pdf
    string lumi_calc_2017_loc = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"; // lumi-calc
    string lumi_calc_2017 = PathResolverFindCalibFile(lumi_calc_2017_loc); // FindDataFile
    if(lumi_calc_2017=="") {
        cout << __hhfunc__ << "    ERROR PathResolver unable to find "
            << "2017 ilumicalc file (=" << lumi_calc_2017_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2017);
    
    // dantrim 2018/10/28 - for n0305 i do not expect to launch mc16e samples, so do not load the 2018 mu profiles (in this way we can
    // get the correct combined mc16a+mc16d PRW for compairing to 2015+2016+2018 data.
    // 2018
    //string lumi_calc_2018_loc = "GoodRunsLists/data18_13TeV/20180924/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root";
    string lumi_calc_2018_loc = "GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root";
    string lumi_calc_2018 = PathResolverFindCalibFile(lumi_calc_2018_loc);
    if(lumi_calc_2018=="") {
        cout << __hhfunc__ << "    ERROR PathResolver unable to find "
            << "2018 lumicalc file (=" << lumi_calc_2018_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2018);
    
    if(!lumi_calc_ok) exit(1);
    
    cout << __hhfunc__ << "    Loading " << lumi_calcs.size() << " lumicalc files" << endl;
    for(auto f : lumi_calcs) {
        cout << __hhfunc__ << "     > " << f << endl;
    }
    
    return lumi_calcs;

}
//____________________________________________________________________________
vector<string> WWbb2Loop::prw_files()
{
//    const xAOD::EventInfo* ei = 0;
//    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
//        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
//        exit(1);
//    }
//    int dsid = ei->mcChannelNumber();
//    string sim_type = "FS"; // hardcoding!
//    string mc_campaign = "mc16d";
//    string prw_config_file_loc = "dev/SUSYTools/PRW_AUTOCONFIG_SIM/files/";
//    if(muon_jet())
//    {
//        prw_config_file_loc = "dev/PileupReweighting/mc16_13TeV/";
//        sim_type = "AFII";
//        mc_campaign = "mc16d";
//        dsid = 346218;
//    }
//    string prw_config = PathResolverFindCalibDirectory(prw_config_file_loc);
//    if(prw_config == "")
//    {
//        cout << __hhfunc__ << "  WARNING could not locate group are for PRW config (=" << prw_config_file_loc << ")" << endl;
//        exit(1);
//    }
//
//    string prw_config_a = prw_config;   
//    prw_config_a += "pileup_" + mc_campaign + "_dsid" + std::to_string(dsid) + "_" + sim_type + ".root";
//    cout << "\n" << __hhfunc__ << "  ======================================================" << endl;
//    cout << __hhfunc__ << "  Loading PRW configs:" << endl;
//    cout << __hhfunc__ << "   > " << prw_config_a << endl;
//
//    bool file_ok = true;
//    TFile test(prw_config_a.data(), "read");
//    if(test.IsZombie())
//    {
//        cout << __hhfunc__ << "  Unable to open PRW config file (=" << prw_config_a << ")" << endl;
//        file_ok = false;
//    }
//    else
//    {
//        cout << __hhfunc__ << "  PRW config opened:" << endl;
//        test.Print();
//        test.ls();
//    }
//    cout << __hhfunc__ << "  ======================================================\n" << endl;
//    if(!file_ok) exit(1);
//
//    vector<string> prw_out = { prw_config_a };
//    return prw_out;
//
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }

    bool file_ok = true;
    vector<string> prw_out;
    int dsid = ei->mcChannelNumber();
    //string sim_type = (af2() ? "AFII" : "FS");
    //string sim_type = "FS"; // dantrim hardcode
    string sim_type = (m_is_af2 ? "AFII" : "FS");
    
    stringstream dsid_s;
    dsid_s << dsid;
    string first3_of_dsid = dsid_s.str();
    first3_of_dsid = first3_of_dsid.substr(0,3);
    stringstream prw_dir;
    prw_dir << "dev/PileupReweighting/share/";
    prw_dir << "DSID" << first3_of_dsid << "xxx/";
    
    string prw_config_loc = prw_dir.str();
    
    string prw_config = PathResolverFindCalibDirectory(prw_config_loc);
    if(prw_config == "")
    {
        cout << __hhfunc__ << "    WARNING Could not locate "
            "group area (=" << prw_config_loc << "), will try the older one: ";
        prw_config_loc = "dev/PileupReweighting/mc16_13TeV/";
        cout << prw_config_loc << endl;
    
        prw_config = PathResolverFindCalibDirectory(prw_config_loc);
        if(prw_config == "")
        {
            cout << __hhfunc__ << "    ERROR Could not locate "
                "alternative group area (=" << prw_config_loc << "), failed to find"
                " PRW config files for sample!" << endl;
            exit(1);
        }
    }
    
    string prw_config_a = prw_config;
    string prw_config_d = prw_config;
    string prw_config_e = prw_config;
    
    prw_config_a += "pileup_mc16a_dsid" + std::to_string(dsid) + "_" + sim_type + ".root";
    prw_config_d += "pileup_mc16d_dsid" + std::to_string(dsid) + "_" + sim_type + ".root";
    prw_config_e += "pileup_mc16e_dsid" + std::to_string(dsid) + "_" + sim_type + ".root";
    
    cout << "================================================" << endl;
    cout << __hhfunc__ << "PRW CONFIG directory : " << prw_config << endl;
    
    TFile test(prw_config_a.data(), "read");
    if(test.IsZombie()) {
        cout << __hhfunc__ << "    ERROR Unable to open the mc16a PRW config file (=" << prw_config_a << ")" << endl;
        file_ok = false;
    }
    else {
        cout << __hhfunc__ <<  "  >> File opened OK (" << prw_config_a << ")" << endl;
    }
    
    TFile test2(prw_config_d.data(), "read");
    if(test2.IsZombie()) {
        cout << __hhfunc__ << "    ERROR Unable to open the mc16d PRW config file (=" << prw_config_d << ")" << endl;
        file_ok = false;
    }
    else {
        cout << __hhfunc__ << "  >> File opened OK (" << prw_config_d << ")" << endl;
    }
    
    TFile test3(prw_config_e.data(), "read");
    if(test3.IsZombie()) {
        cout << __hhfunc__ << "    ERROR Unable to open the mc16e PRW config file (=" << prw_config_e << ")" << endl;
        file_ok = false;
    }
    else {
        cout << __hhfunc__ << "  >> File opened OK (" << prw_config_e << ")" << endl;
    }
    cout << "================================================" << endl;
    
    if(!file_ok) exit(1);
    
    prw_out.push_back(prw_config_a);
    prw_out.push_back(prw_config_d);
    prw_out.push_back(prw_config_e);

    return prw_out;
}
//____________________________________________________________________________
bool WWbb2Loop::initialize_grl()
{
    stringstream sx;
    sx << "GoodRunsListSelectionTool/WWbbGRL";
    _grl_tool.setTypeAndName(sx.str());
    _grl_tool.isUserConfigured();

    vector<string> grl_files;

    string grl_2015_loc = "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml";
    string grl_2016_loc = "GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
    string grl_2017_loc = "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";
    string grl_2018_loc = "GoodRunsLists/data18_13TeV/20180906/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";

    // now find them
    bool grl_found_ok = true;

    // 2015
    string grl_2015 = PathResolverFindCalibFile(grl_2015_loc);
    if(grl_2015 == "")
    {
        cout << __hhfunc__ << "  ERROR PathResolver unable to find 2015 GRL file (=" << grl_2015_loc << ")" << endl;
        grl_found_ok = false;
    }
    string grl_2016 = PathResolverFindCalibFile(grl_2016_loc);
    if(grl_2016 == "")
    {
        cout << __hhfunc__ << "  ERROR PathResolver unable to find 2016 GRL file (=" << grl_2016_loc << ")" << endl;
        grl_found_ok = false;
    }
    string grl_2017 = PathResolverFindCalibFile(grl_2017_loc);
    if(grl_2017 == "")
    {
        cout << __hhfunc__ << "  ERROR PathResolver unable to find 2017 GRL file (=" << grl_2017_loc << ")" << endl;
        grl_found_ok = false;
    }
    string grl_2018 = PathResolverFindCalibFile(grl_2018_loc);
    if(grl_2018 == "")
    {
        cout << __hhfunc__ << "  ERROR PathResolver unablet o find 2018 GRL file (=" << grl_2018_loc << ")" << endl;
        grl_found_ok = false;
    }

    if(!grl_found_ok)
    {
        return false;
    }

    grl_files.push_back(grl_2015);
    grl_files.push_back(grl_2016);
    grl_files.push_back(grl_2017);
    grl_files.push_back(grl_2018);

    cout << "\n" <<__hhfunc__ << "  =============================================================" << endl;
    cout << __hhfunc__ << "  Initializing GRL with files:" << endl;
    for(auto g : grl_files)
    {
        cout << __hhfunc__ << "    > " << g << endl;
    }
    cout <<__hhfunc__ << "  =============================================================\n" << endl;

    RETURN_CHECK(GetName(), _grl_tool.setProperty("GoodRunsListVec", grl_files));
    RETURN_CHECK(GetName(), _grl_tool.setProperty("PassThrough", false));
    RETURN_CHECK(GetName(), _grl_tool.retrieve());

    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::initialize_susytools()
{
    string name = "SUSYToolsWWbb";
    _susyObj = new ST::SUSYObjDef_xAOD(name);

    cout << __hhfunc__ << "    Initializing SUSYTools..." << endl;

    stringstream st_config;
    st_config << "wwbb2l2x/SUSYTools_WWbb.conf";
    RETURN_CHECK( GetName(), _susyObj->setProperty("ConfigFile", st_config.str()) );
    RETURN_CHECK( GetName(), _susyObj->setProperty("mcCampaign", "mc16a"));
    _susyObj->msg().setLevel(verbose() ? MSG::DEBUG : MSG::WARNING);
    ST::ISUSYObjDef_xAODTool::DataSource datasource = ST::ISUSYObjDef_xAODTool::FullSim;
    //ST::ISUSYObjDef_xAODTool::DataSource datasource = ST::ISUSYObjDef_xAODTool::AtlfastII;
    RETURN_CHECK( GetName(), _susyObj->setProperty("DataSource", datasource) );

    auto lumicalcs = ilumicalc_files();
    auto prw_configs = prw_files();
    cout << "\n===================================================" << endl;
    cout << __hhfunc__ << "  Loading ilumicalc files into SUSYObj:" << endl;
    for(auto calc : lumicalcs)
    {
        cout << __hhfunc__ << "    > " << calc << endl;
    }
    cout << __hhfunc__ << "  Loading prw config files into SUSYObj:" << endl;
    for(auto conf : prw_configs)
    {
        cout << __hhfunc__ << "   > " << conf << endl;
    }
    cout << "===================================================\n" << endl;
    RETURN_CHECK( GetName(), _susyObj->setProperty("PRWLumiCalcFiles", lumicalcs) );
    RETURN_CHECK( GetName(), _susyObj->setProperty("PRWConfigFiles", prw_configs) );

    if(_susyObj->initialize() != StatusCode::SUCCESS) {
        cout << __hhfunc__ << "   ERROR Cannot initialize SUSYTools, exitting." << endl;
        return false;
    }
    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::load_objects()
{

    RETURN_CHECK(GetName(), susyObj()->GetElectrons(_electrons, _electrons_aux, true));
    RETURN_CHECK(GetName(), susyObj()->GetMuons(_muons, _muons_aux, true));
    RETURN_CHECK(GetName(), susyObj()->GetJets(_jets, _jets_aux));
    RETURN_CHECK(GetName(), susyObj()->GetPhotons(_photons, _photons_aux, true));
    RETURN_CHECK(GetName(), susyObj()->GetTaus(_taus, _taus_aux));

    for(const auto & j : *xaod_jets())
    {
        susyObj()->IsBJet(*j);
    }

    RETURN_CHECK(GetName(), susyObj()->OverlapRemoval(xaod_electrons(), xaod_muons(), xaod_jets()));

    for(const auto & e : *xaod_electrons())
    {
        dec_charge(*e) = e->charge();
        if(acc_baseline(*e) && acc_passOR(*e)) _base_electrons.push_back(e);
        if(acc_signal(*e) && acc_passOR(*e)) _sig_electrons.push_back(e);
    }
    std::sort(_base_electrons.begin(), _base_electrons.end(), PtGreater);
    std::sort(_sig_electrons.begin(), _sig_electrons.end(), PtGreater);

    for(const auto & m : *xaod_muons())
    {
        dec_charge(*m) = m->charge();
        _pre_muons.push_back(m);
        if(acc_baseline(*m) && acc_passOR(*m)) _base_muons.push_back(m);
        if(acc_signal(*m) && acc_passOR(*m)) _sig_muons.push_back(m);
    }
    std::sort(_pre_muons.begin(), _pre_muons.end(), PtGreater);
    std::sort(_base_muons.begin(), _base_muons.end(), PtGreater);
    std::sort(_sig_muons.begin(), _sig_muons.end(), PtGreater);

    for(const auto & lep : _base_electrons)
    {
        _base_leptons.push_back(lep);
    }
    for(const auto & lep : _base_muons)
    {
        _base_leptons.push_back(lep);
    }
    std::sort(_base_leptons.begin(), _base_leptons.end(), PtGreater);

    for(const auto & lep : _sig_electrons)
    {
        _sig_leptons.push_back(lep);
    }
    for(const auto & lep : _sig_muons)
    {
        _sig_leptons.push_back(lep);
    }
    std::sort(_sig_leptons.begin(), _sig_leptons.end(), PtGreater);

    for(const auto & j : *xaod_jets())
    {
        if(acc_baseline(*j)) _base_jets.push_back(j);
        if(acc_signal(*j) && acc_passOR(*j)) _sig_jets.push_back(j);
    }
    std::sort(_base_jets.begin(), _base_jets.end(), PtGreaterJet);
    std::sort(_sig_jets.begin(), _sig_jets.end(), PtGreaterJet);

    _met = new xAOD::MissingETContainer();
    _met_aux = new xAOD::MissingETAuxContainer();
    _met->setStore(_met_aux);
    _met->reserve(10);

    susyObj()->GetMET(*_met,
                _jets,
                _electrons,
                _muons,
                _photons,
                0);
                

    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::clear_objects()
{
    store()->clear();

    _electrons = 0;
    _electrons_aux = 0;
    _muons = 0;
    _muons_aux = 0;
    _jets = 0;
    _jets_aux = 0;
    _photons = 0;
    //_photons_aux = 0;
    _taus = 0;
    _taus_aux = 0;

    _base_electrons.clear();
    _sig_electrons.clear();
    _pre_muons.clear();
    _base_muons.clear();
    _sig_muons.clear();
    _base_leptons.clear();
    _sig_leptons.clear();
    _base_jets.clear();
    _sig_jets.clear();
    _base_photons.clear();
    _sig_photons.clear();
    _base_taus.clear();
    _sig_taus.clear();

    if(_met)
    {
        delete _met;
        _met = 0;
    }
    if(_met_aux)
    {
        delete _met_aux;
        _met_aux = 0;
    }

    return true;
}
//____________________________________________________________________________
void WWbb2Loop::Begin(TTree* /*tree*/)
{
    cout << __hhfunc__ << endl;
    timer()->Start();
}
//____________________________________________________________________________
void WWbb2Loop::Terminate()
{
    cout << "\n========================================================================\n" << endl;
    cout << __hhfunc__ << " Looping complete" << endl;
    timer()->Stop();

    if(muon_jet())
    {
        write_muon_jet_outputs();
    }

    if(do_uncerts())
    {
        output_file()->cd();
        output_tree()->Write();
        cout << timer_summary() << endl;
    }
    else if(!muon_jet())
    {
        print_cutflow();
    }
    cout << "\n========================================================================\n" << endl;

    delete _rtree;
    delete _rfile;
}
//____________________________________________________________________________
string WWbb2Loop::timer_summary()
{
    double realTime = m_timer.RealTime();
    double cpuTime = m_timer.CpuTime();
    int hours = int(realTime / 3600);
    realTime -= hours * 3600;
    int min = int(realTime / 60);
    realTime -= min * 60;
    int sec = int(realTime);
    int nEventInput = n_events_processed;
    int nEventOutput = output_tree() ? output_tree()->GetEntries() : -1;
    float speed = nEventInput / m_timer.RealTime()/1000;
    TString line1; line1.Form("Real %d:%02d:%02d, CPU %.3f", hours, min, sec, cpuTime);
    TString line2; line2.Form("%2.3f",speed);
    ostringstream oss;
    oss << __hhfunc__ << "    -------------------------------------------------------------" << endl;
    oss << __hhfunc__ <<"      Number of events processed : " << nEventInput << endl
        << __hhfunc__ <<"      Number of events saved     : " << nEventOutput << endl
        << __hhfunc__ <<"      Analysis time              : " << line1 << endl
        << __hhfunc__ <<"      Analysis speed [kHz]       : " << line2 << endl;
    oss << __hhfunc__ <<"    -------------------------------------------------------------" << endl;
    return oss.str();
}
//____________________________________________________________________________
bool WWbb2Loop::prepare_for_event(Long64_t entry_to_read)
{
    if(entry_to_read==0) {
        cout << "\n========================================================================" << endl;
        cout << __hhfunc__ << "  Prepare For Looping" << endl;
    }
    clear_objects();
    event()->getEntry(entry_to_read);
    susyObj()->ApplyPRWTool();
    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::pass_event_cleaning()
{
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }

    // GRL
    bool pass_grl = (mc() ? true : _grl_tool->passRunLB(ei->runNumber(), ei->lumiBlock()));
    if(!pass_grl) return false;
    pass_cut("GRL");

    // TTCVeto
    bool pass_TTCVeto = ei->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ? false : true;
    if(!pass_TTCVeto) return false;

    // LArError
    bool pass_LAr = (ei->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ? false : true;
    if(!pass_LAr) return false;

    // TileError
    bool pass_tile = (ei->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ? false : true;
    if(!pass_tile) return false;

    // SCTError
    bool pass_sct = (ei->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ? false : true;
    if(!pass_sct) return false;
    pass_cut("EVENTCLEANING");

    // GoodVtx
    bool pass_good_vtx = (susyObj()->GetPrimVtx() == nullptr) ? false : true;
    if(!pass_good_vtx) return false;
    pass_cut("GOODVERTEX");

    // Bad Muon
    bool pass_bad_mu = true;
    for(const auto & mu : base_muons())
    {
        if(acc_bad(*mu)) pass_bad_mu = false;
    }
    if(!pass_bad_mu) return false;
    pass_cut("BADMUON");

    // Cosmic Muon
    bool pass_cosmic = true;
    //size_t mu_idx = 0;
    //cout << "==========" << endl;
    //cout << " EVENT " << ei->eventNumber() << endl;

    ///int evnum = ei->eventNumber();
    ///if(evnum == 15127004)
    ///{
    ///    size_t mu_idx = 0;
    ///    for(const auto & mu : pre_muons())
    ///    {
    ///        cout << "EVENT " << evnum << " mu["<<mu_idx<<"] (pT,eta)=("<<(mu->pt()*1e-3)<<","<<mu->eta()<<") passOR?: " << (unsigned)acc_passOR(*mu) <<" isCosmic?: " << (unsigned)acc_cosmic(*mu) << endl;
    ///        mu_idx++;
    ///    }
    ///}

    for(const auto & mu : base_muons())
    {
        //cout << "COSMIC  mu[" << mu_idx << "] (pT,eta)=("<<(mu->pt()*1e-3)<<","<<mu->eta()<<") passOR? : " << (unsigned)acc_passOR(*mu) << " cosmic? : " << (unsigned)acc_cosmic(*mu) << endl;
        if(acc_passOR(*mu) && acc_cosmic(*mu)) pass_cosmic = false;
    }
    //cout << "EVENT " << ei->eventNumber() << " COSMIC " << pass_cosmic << endl;
    if(!pass_cosmic) return false;
    pass_cut("COSMICMUON");

    // jet cleaning
    bool pass_jet_cleaning = true;
    for(const auto & jet : base_jets())
    {
        if(acc_passOR(*jet) && acc_bad(*jet)) pass_jet_cleaning = false;
    }
    if(!pass_jet_cleaning) return false;
    pass_cut("JETCLEANING");
    
    return true;
}
//____________________________________________________________________________
bool WWbb2Loop::lepton_is_electron(const xAOD::IParticle* particle)
{
    if(particle->type() == xAOD::Type::Electron)
    {
 //       auto lep = dynamic_cast<const xAOD::Electron*>(particle);
//        dec_charge(*particle) = lep->charge();
        return true;
    }
    return false;
}
//____________________________________________________________________________
bool WWbb2Loop::lepton_is_muon(const xAOD::IParticle* particle)
{
    if(particle->type() == xAOD::Type::Muon)
    {
        return true;
    }
    return false;
}
//____________________________________________________________________________
bool WWbb2Loop::pass_flavor(const vector< xAOD::IParticle*> leptons, string flav)
{
    bool lead_ele = lepton_is_electron(leptons.at(0));
    bool lead_mu = lepton_is_muon(leptons.at(0));
    bool sub_ele = lepton_is_electron(leptons.at(1));
    bool sub_mu = lepton_is_muon(leptons.at(1));

    if(flav == "ee")
    {
        if(lead_ele && sub_ele && !lead_mu && !sub_mu) return true;
        else return false;
    }
    else if(flav == "mm")
    {
        if(!lead_ele && !sub_ele && lead_mu && sub_mu) return true;
        else return false;
    }
    else if(flav == "em")
    {
        if( (lead_ele && !lead_mu) && (sub_mu && !sub_ele) ) return true;
        else return false;
    }
    else if(flav == "me")
    {
        if( (lead_mu && !lead_ele) && (sub_ele && !sub_mu) ) return true;
        else return false;
    }
    else if(flav == "df")
    {
        if( (lead_ele && !lead_mu && !sub_ele && sub_mu) || (!lead_ele && lead_mu && sub_ele && !sub_mu)) return true;
        else return false;
    }
    else {
        cout << __hhfunc__ << "  Unknown dilepton flavor attempted!" << endl;
        exit(1);
    }
    return false;
}
//____________________________________________________________________________
bool WWbb2Loop::pass_zveto(const vector<xAOD::IParticle*> leptons, float width)
{
    float is_sf = (pass_flavor(leptons, "ee") || pass_flavor(leptons, "mm"));
    if(!is_sf) return true;
    float mll = (leptons.at(0)->p4() + leptons.at(1)->p4()).M() * 1e-3;
    return fabs( mll - 91.2 ) > width;
}
//____________________________________________________________________________
void WWbb2Loop::test_cutflow()
{
    std::map< std::string, std::string > trigger_map
    {
        {"ee", "HLT_2e17_lhvloose_nod0"},
        {"mm", "HLT_mu22_mu8noL1"},
        {"df", "HLT_e17_lhloose_nod0_mu14"}
    };

    // pass the trigger?
    bool trig_fired = susyObj()->IsTrigPassed( trigger_map.at(do_flav()) );
    if(!trig_fired)
    {
        return;
    }
    pass_cut("TRIGGER");


    // at least two baseline leptons
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }
    bool pass_mult = (base_leptons().size()>=2);
    if(!pass_mult) return;
    bool pass_flav = pass_flavor(base_leptons(), do_flav());
    if(!pass_flav) return;
    //bool pass_it = (pass_flav && pass_mult);

    pass_cut("BASELINELEPTON");

    //int evnum = ei->eventNumber();
    //if(evnum == 19975427 || evnum == 19971996 || evnum == 15124980)
    //{
    //    cout << "EVENT " << evnum << " : ";
    //    for(size_t i = 0; i < sig_leptons().size(); i++)
    //    {
    //        auto l = sig_leptons().at(i);
    //        float z0 = acc_z0sinTheta(*l);
    //        float d0 = acc_d0sig(*l);
    //        cout << " ["<<i<<"] (pT,eta)=("<<(l->pt()*1e-3)<<","<<l->eta()<<") (d0SigBSCorr,z0SinTheta)=("<<fabs(d0)<<","<<fabs(z0)<<") - ";
    //    }
    //    cout << endl;
    //}


    // exactly two signal leptons
    bool n_sig = (sig_leptons().size()==2);
    if(!n_sig) return;
    pass_flav = pass_flavor(sig_leptons(), do_flav());
    if(!pass_flav) return;
    //cout << "EVENT " << ei->eventNumber() << " : "; 
    //for(int lep_idx = 0; lep_idx < sig_leptons().size(); lep_idx++)
    //{
    //    auto e = sig_leptons().at(lep_idx);
    //    cout << " ["<<lep_idx<<"] isEle? " << ((e->type() == xAOD::Type::Electron) ? 1 : 0) << " (pT,eta)=("<<(e->pt()*1e-3)<<","<<e->phi()<<") ";
    //} 
    //cout << endl;
    pass_cut("SIGNALLEPTON");

    // opposite charge
    bool pass_os = (acc_charge(*sig_leptons().at(0)) != acc_charge(*sig_leptons().at(1)));
    if(!pass_os) return;
    pass_cut("OPPOSITECHARGE");

    // mll > 20 GeV
    float mll = (sig_leptons().at(0)->p4() + sig_leptons().at(1)->p4()).M() * 1e-3;
    if(!(mll>20.)) return;
    pass_cut("MLL20");

    // Z-veto
    bool pass_z = pass_zveto(sig_leptons(), 20.);
    if(!pass_z) return;
    pass_cut("ZVETO");

    // b-jets
    vector<const xAOD::Jet*> bjets;
    for(const auto & j : sig_jets())
    {
        if(susyObj()->IsBJet(*j)) bjets.push_back(j);
    }
    if(!(bjets.size()==2)) return;
    pass_cut("BJETMULT");

    // mbb in higgs window
    float mbb = (bjets.at(0)->p4() + bjets.at(1)->p4()).M() * 1e-3;
    bool pass_higgs = (mbb>100 && mbb<140);
    if(!pass_higgs) return;
    pass_cut("HIGGSMBB");
    
}
//____________________________________________________________________________
void WWbb2Loop::print_cutflow()
{
    cout << __hhfunc__ << "  =====================================================" << endl;
    cout << __hhfunc__ << "   Cutflow (dilepton flavor = " << do_flav() << ")\n" << endl;
    int cut_idx = 0;
    for(auto cutname : _cut_names)
    {
        cout << __hhfunc__ << " [" << cut_idx << "] " << cutname << " : "
            << std::setw(10) << _raw_cut_map[cutname] << endl;
        cut_idx++;
    } // cutname
    cout << __hhfunc__ << "  =====================================================" << endl;
}
//____________________________________________________________________________
void WWbb2Loop::write_muon_jet_outputs()
{
    TFile* f = new TFile("muon_jet_outputs.root", "RECREATE");
    if(f->IsZombie())
    {
        cout << __hhfunc__ << " ERROR Could not open output file for storing muon-jet outputs" << endl;
        delete f;
        f = 0;
        return;
    }
    f->cd();

    h_n_dR1->Write();
    h_n_dR04->Write();
    h_n_dR02->Write();

    h_mbb_uncorr->Write();
    h_mbb_corr->Write();

    f->Write();

    
}
//____________________________________________________________________________
void WWbb2Loop::do_muon_jet()
{
    vector<TLorentzVector> muons;
    vector<TLorentzVector> nearby_muons;
    vector<TLorentzVector> bjets;
    vector<TLorentzVector> corrected_bjets;

    for(const auto & mu : *xaod_muons())
    {
        if(acc_baseline(*mu)) muons.push_back(mu->p4());
    } // mu
    for(const auto & jet : *xaod_jets())
    {
        if(acc_bjet(*jet)) bjets.push_back(jet->p4());
    }

    

    vector<int> muons_added;
    for(auto bjet : bjets)
    {
        int n_1 = 0;
        int n_04 = 0;
        int n_02 = 0;
        for(size_t imu = 0; imu < muons.size(); imu++)
        {
            auto mu = muons.at(imu);
            if(bjet.DeltaR(mu) < 0.1) n_1++;
            if(bjet.DeltaR(mu) < 0.4) n_04++;
            if(bjet.DeltaR(mu) < 0.4)
            {
                n_02++;
                if(std::find(muons_added.begin(), muons_added.end(), (int)imu) == muons_added.end())
                {
                    //TLorentzVector bj_corr = mu + bjet;
                    //float initial_pt = bjet.Pt() * 1e-3;
                    bjet = mu + bjet;
                    //float final_pt = bjet.Pt() * 1e-3;
                    //cout << "added muon to jet: initial Pt = " << initial_pt << ", final Pt = " << final_pt << endl;
                    //corrected_bjets.push_back(bj_corr);
                    muons_added.push_back((int)imu);
                }
            }
        } // imu

        h_n_dR1->Fill(n_1);
        h_n_dR04->Fill(n_04);
        h_n_dR02->Fill(n_02);

        corrected_bjets.push_back(bjet);
    }

    if(bjets.size() >= 2)
    {
        TLorentzVector bb = (bjets.at(0) + bjets.at(1));
        TLorentzVector bb_corr = (corrected_bjets.at(0) + corrected_bjets.at(1));
        h_mbb_uncorr->Fill(bb.M() * 1e-3);
        h_mbb_corr->Fill(bb_corr.M() * 1e-3);
    }

    

}
//____________________________________________________________________________
void WWbb2Loop::print_weights()
{
    cout << __hhfunc__ << "    ==========================================================" << endl;
    size_t widx = 0;
    auto weight_names = _weightTool->getWeightNames();
    size_t n_weights = weight_names.size();
    for(auto name : weight_names)
    {
        cout << __hhfunc__ << "     > [" << (widx) << " / " << (n_weights) << "] " << name << endl;
        widx++;
    }
    cout << __hhfunc__ << "    ==========================================================" << endl;
    exit(1);
}
//____________________________________________________________________________
Bool_t WWbb2Loop::Process(Long64_t /*entry*/)
{
    static Long64_t chain_entry = -1;
    chain_entry++;
    prepare_for_event(chain_entry);
    n_events_processed++;

    if(verbose() || chain_entry % 500 == 0) {
        cout << __hhfunc__ << "    **** Processing entry " << setw(6) << chain_entry << " **** " << endl;
    }

    if(chain_entry == 0 && do_print_weights())
    {
        print_weights();
    }

    pass_cut("READIN");

    if(!load_objects()) {
        cout << __hhfunc__ << "    ERROR Failed to load physics objects" << endl;
        return false;
    }

    // run standard ATLAS event cleaning
    if(!pass_event_cleaning()) return true;

    if(muon_jet())
    {
        do_muon_jet();
        return true;
    }
    else if(do_uncerts())
    {
        process_uncerts();
    }
    else
    {
        test_cutflow();
    }

    //////////////////////////////////////////
    //  end of event loop
    return true;
}
//____________________________________________________________________________
void WWbb2Loop::process_uncerts()
{

    // require exactly 2 signal leptons
    if(sig_leptons().size() != 2) return;

    // require at least 2 b-tagged jets
    vector<const xAOD::Jet*> bjets;
    vector<const xAOD::Jet*> sjets;
    for(const auto & j : sig_jets())
    {
        if(susyObj()->IsBJet(*j)) bjets.push_back(j);
        else{ sjets.push_back(j); }
    }
    if(!(bjets.size()>=2)) return;

    b_nom_sumw = m_sumOfWeights;
    b_n_in_aod = m_nEventsProcessed;

    b_nJets = sig_jets().size();
    b_nBJets = bjets.size();
    b_nSJets = sjets.size();

    if(b_nJets>0)
    {
        b_j0_pt = sig_jets().at(0)->pt() * 1e-3;
        b_j0_eta = sig_jets().at(0)->eta();
        b_j0_phi = sig_jets().at(0)->phi();

        if(b_nJets>1)
        {
            b_j1_pt  = sig_jets().at(1)->pt() * 1e-3;
            b_j1_eta = sig_jets().at(1)->eta();
            b_j1_phi = sig_jets().at(1)->phi();

            if(b_nJets>2)
            {
                b_j2_pt  = sig_jets().at(2)->pt() * 1e-3;
                b_j2_eta = sig_jets().at(2)->eta();
                b_j2_phi = sig_jets().at(2)->phi();
            }
            else{ b_j2_pt = 0; b_j2_eta = -99.; b_j2_phi = -99.; }
        }
        else { b_j1_pt = 0; b_j1_eta = -99.; b_j1_phi = -99; }
    }
    else{ b_j0_pt = 0; b_j0_eta = -99.; b_j0_phi = -99; }

    if(b_nBJets>0)
    {
        b_bj0_pt =  bjets.at(0)->pt() * 1e-3;
        b_bj0_eta = bjets.at(0)->eta();
        b_bj0_phi = bjets.at(0)->phi();

        if(b_nBJets>1)
        {
            b_bj1_pt  = bjets.at(1)->pt() * 1e-3;
            b_bj1_eta = bjets.at(1)->eta();
            b_bj1_phi = bjets.at(1)->phi();

            if(b_nBJets>2)
            {
                b_bj2_pt  = bjets.at(2)->pt() * 1e-3;
                b_bj2_eta = bjets.at(2)->eta();
                b_bj2_phi = bjets.at(2)->phi();
            }
            else{ b_bj2_pt = 0; b_bj2_eta = -99.; b_bj2_phi = -99.; }
        }
        else { b_bj1_pt = 0; b_bj1_eta = -99.; b_bj1_phi = -99; }
    }
    else{ b_bj0_pt = 0; b_bj0_eta = -99.; b_bj0_phi = -99; }

    if(b_nSJets>0)
    {
        b_sj0_pt =  sjets.at(0)->pt() * 1e-3;
        b_sj0_eta = sjets.at(0)->eta();
        b_sj0_phi = sjets.at(0)->phi();

        if(b_nSJets>1)
        {
            b_sj1_pt  = sjets.at(1)->pt() * 1e-3;
            b_sj1_eta = sjets.at(1)->eta();
            b_sj1_phi = sjets.at(1)->phi();

            if(b_nSJets>2)
            {
                b_sj2_pt  = sjets.at(2)->pt() * 1e-3;
                b_sj2_eta = sjets.at(2)->eta();
                b_sj2_phi = sjets.at(2)->phi();
            }
            else{ b_sj2_pt = 0; b_sj2_eta = -99.; b_sj2_phi = -99.; }
        }
        else { b_sj1_pt = 0; b_sj1_eta = -99.; b_sj1_phi = -99; }
    }
    else{ b_sj0_pt = 0; b_sj0_eta = -99.; b_sj0_phi = -99; }

    // clear the weights vector for the incoming event
    b_weights->clear();
    b_weight_names->clear();
    //b_weight_deltas->clear();
    // fill weights
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess()) {
        cout << __hhfunc__ << "  ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }
    b_nom_weight = ei->mcEventWeight();
    b_dsid = ei->mcChannelNumber();
    if(m_mctype == "mc16a") b_mctype = 0;
    else if(m_mctype == "mc16d") b_mctype = 1;
    else if(m_mctype == "mc16e") b_mctype = 2;
    else {
        cout << __hhfunc__ << "    ERROR Unknown mctype (=" << m_mctype << ")" << endl;
        exit(1);
    }
    for(auto weight_name : _weightTool->getWeightNames())
    {
        b_weight_names->push_back(weight_name);
        auto var = _weightTool->getWeight(weight_name);
        b_weights->push_back(var);
        //float delta = (var - b_nom_weight) / b_nom_weight;
        //b_weight_deltas->push_back(delta);
    }

    b_l0_pt = sig_leptons().at(0)->p4().Pt() * 1e-3;
    b_l1_pt = sig_leptons().at(1)->p4().Pt() * 1e-3;
    b_l0_eta = sig_leptons().at(0)->eta();
    b_l1_eta = sig_leptons().at(1)->eta();
    b_l0_phi = sig_leptons().at(0)->phi();
    b_l1_phi = sig_leptons().at(1)->phi();

    TLorentzVector ll = (sig_leptons().at(0)->p4() + sig_leptons().at(1)->p4());
    b_pTll = ll.Pt() * 1e-3;
    b_mll = ll.M() * 1e-3;
    b_dphi_ll = sig_leptons().at(0)->p4().DeltaPhi(sig_leptons().at(1)->p4());
    b_dRll = sig_leptons().at(0)->p4().DeltaR(sig_leptons().at(1)->p4());

    // Get MET
    auto met_it = xaod_met()->find("Final");
    if(met_it == xaod_met()->end())
    {
        cout << __hhfunc__ << "   ERROR No RefFinal MET inside MET container" << endl;
        exit(1);
    }
    TLorentzVector metTLV;
    metTLV.SetPtEtaPhiE( (*met_it)->met(), 0, (*met_it)->phi(), (*met_it)->met());

    b_met = metTLV.Pt() * 1e-3;
    b_metPhi = metTLV.Phi();

    b_met_pTll = (ll + metTLV).Pt() * 1e-3;
    b_dphi_met_ll = ll.DeltaPhi(metTLV);

    TLorentzVector bb = (bjets.at(0)->p4() + bjets.at(1)->p4());
    b_mbb = bb.M() * 1e-3;
    b_dphi_bb = bjets.at(0)->p4().DeltaPhi(bjets.at(1)->p4());
    b_dRbb = bjets.at(0)->p4().DeltaR(bjets.at(1)->p4());

    b_dR_ll_bb = ll.DeltaR(bb);
    b_dphi_ll_bb = ll.DeltaPhi(bb);

    if(b_nJets>0)
    {
        b_dphi_j0_ll = ll.DeltaPhi(sig_jets().at(0)->p4());
        b_dphi_j0_l0 = sig_leptons().at(0)->p4().DeltaPhi(sig_jets().at(0)->p4());
    }
    if(b_nSJets>0)
    {
        b_dphi_sj0_ll = ll.DeltaPhi(sjets.at(0)->p4());
        b_dphi_sj0_l0 = sig_leptons().at(0)->p4().DeltaPhi(sjets.at(0)->p4());
    }
    if(b_nBJets>0)
    {
        b_dphi_bj0_ll = ll.DeltaPhi(bjets.at(0)->p4());
        b_dphi_bj0_l0 = sig_leptons().at(0)->p4().DeltaPhi(bjets.at(0)->p4());
    }

    ComputeMT2 calc_mt2ll = ComputeMT2(sig_leptons().at(0)->p4(), sig_leptons().at(1)->p4(), metTLV);
    b_mt2_ll = calc_mt2ll.Compute() * 1e-3;

    ComputeMT2 calc_mt2bb = ComputeMT2(bjets.at(0)->p4(), bjets.at(1)->p4(), metTLV);
    b_mt2_bb = calc_mt2bb.Compute() * 1e-3;

    ComputeMT2 calc_mt2_llbb = ComputeMT2(ll, bb, metTLV);
    b_mt2_llbb = calc_mt2_llbb.Compute() * 1e-3;

    // HT2
    float num_ht2 = bb.Pt() + (ll + metTLV).Pt();
    float den_ht2 = bjets.at(0)->p4().Pt() + bjets.at(1)->p4().Pt() + sig_leptons().at(0)->p4().Pt() + sig_leptons().at(1)->p4().Pt() + metTLV.Pt();
    b_HT2 = num_ht2 * 1e-3;
    b_HT2Ratio = num_ht2 / den_ht2;

    // MT_1
    TLorentzVector vis = (ll + bb);
    double pt_vis = vis.Pt();
    double m_vis = vis.M();
    double et_vis = sqrt(pt_vis * pt_vis + m_vis * m_vis);
    double x = et_vis + metTLV.Pt();
    double y = (vis + metTLV).Pt();
    b_MT_1 = sqrt( x * x - y * y ) * 1e-3;

    // MT_1_scaled
    double mbb = bb.M() * 1e-3;
    double scaling = 125.09 / mbb;
    TLorentzVector bb_scaled;
    bb_scaled.SetPtEtaPhiE( bb.Pt() * scaling, bb.Eta(), bb.Phi(), bb.E() * scaling);
    TLorentzVector vis_scaled = (ll + bb_scaled);
    double pt_vis_scaled = vis_scaled.Pt();
    double m_vis_scaled = vis_scaled.M();
    double et_vis_scaled = sqrt( pt_vis_scaled * pt_vis_scaled + m_vis_scaled * m_vis_scaled);
    double x_sc = et_vis_scaled +  metTLV.Pt();
    double y_sc = (vis_scaled + metTLV).Pt();
    b_MT_1_scaled = sqrt( x_sc * x_sc - y_sc * y_sc ) * 1e-3;

    bool is_ee = pass_flavor(sig_leptons(), "ee");
    bool is_mm = pass_flavor(sig_leptons(), "mm");
    bool is_em = pass_flavor(sig_leptons(), "em");
    bool is_me = pass_flavor(sig_leptons(), "me");
    bool is_df = pass_flavor(sig_leptons(), "df");
    bool is_sf = !is_df;

    b_isEE = (is_ee ? 1 : 0);
    b_isMM = (is_mm ? 1 : 0);
    b_isEM = (is_em ? 1 : 0);
    b_isME = (is_me ? 1 : 0);
    b_isSF = (is_sf ? 1 : 0);
    b_isDF = (is_df ? 1 : 0);

    // NN
    _lwt_map.clear();
    _lwt_map["isEE"] = b_isEE;
    _lwt_map["isMM"] = b_isMM;
    _lwt_map["isEM"] = b_isEM;
    _lwt_map["isME"] = b_isME;
    _lwt_map["isSF"] = b_isSF;
    _lwt_map["isDF"] = b_isDF;
    _lwt_map["l0_pt"] = b_l0_pt;
    _lwt_map["l1_pt"] = b_l1_pt;
    _lwt_map["l0_eta"] = b_l0_eta;
    _lwt_map["l1_eta"] = b_l1_eta;
    _lwt_map["l0_phi"] = b_l0_phi;
    _lwt_map["l1_phi"] = b_l1_phi;
    _lwt_map["mll"] = b_mll;
    _lwt_map["pTll"] = b_pTll;
    _lwt_map["dphi_ll"] = b_dphi_ll;
    _lwt_map["nJets"] = b_nJets;
    _lwt_map["nSJets"] = b_nSJets;
    _lwt_map["nBJets"] = b_nBJets;
    _lwt_map["j0_pt"] = b_j0_pt;
    _lwt_map["j1_pt"] = b_j1_pt;
    _lwt_map["bj0_pt"] = b_bj0_pt;
    _lwt_map["bj1_pt"] = b_bj1_pt;
    _lwt_map["j0_eta"] = b_j0_eta;
    _lwt_map["j1_eta"] = b_j1_eta;
    _lwt_map["bj0_eta"] = b_bj0_eta;
    _lwt_map["bj1_eta"] = b_bj1_eta;
    _lwt_map["j0_phi"] = b_j0_phi;
    _lwt_map["j1_phi"] = b_j1_phi;
    _lwt_map["bj0_phi"] = b_bj0_phi;
    _lwt_map["bj1_phi"] = b_bj1_phi;
    _lwt_map["dphi_bj0_ll"] = b_dphi_bj0_ll;
    _lwt_map["dphi_bj0_l0"] = b_dphi_bj0_l0;
    _lwt_map["met"] = b_met;
    _lwt_map["metPhi"] = b_metPhi;
    _lwt_map["dphi_met_ll"] = b_dphi_met_ll;
    _lwt_map["mt2"] = b_mt2_ll;
    _lwt_map["dRll"] = b_dRll;
    _lwt_map["mbb"] = b_mbb;
    _lwt_map["met_pTll"] = b_met_pTll;
    _lwt_map["HT2"] = b_HT2;
    _lwt_map["HT2Ratio"] = b_HT2Ratio;
    _lwt_map["mt2_llbb"] = b_mt2_llbb;
    _lwt_map["mt2_bb"] = b_mt2_bb;
    _lwt_map["dphi_bb"] = b_dphi_bb;

    std::map<std::string, std::map<std::string, double> > inputs;
    inputs["InputLayer"] = _lwt_map;
    auto output_scores = _lwt->compute(inputs);

    b_NN_p_hh = output_scores.at("out_0_hh");
    b_NN_p_top = output_scores.at("out_1_top");
    b_NN_p_zsf = output_scores.at("out_2_zsf");
    b_NN_p_ztt = output_scores.at("out_3_ztt");

    b_NN_d_hh = log( b_NN_p_hh / (b_NN_p_top + b_NN_p_zsf + b_NN_p_ztt) );
    b_NN_d_top = log( b_NN_p_top / (b_NN_p_hh + b_NN_p_zsf + b_NN_p_ztt) );
    b_NN_d_zsf = log( b_NN_p_zsf / (b_NN_p_hh + b_NN_p_top + b_NN_p_ztt) );
    b_NN_d_ztt = log( b_NN_p_ztt / (b_NN_p_hh + b_NN_p_top + b_NN_p_zsf) );

    bool fill_output = true;
    if(do_slim())
    {
        bool pass_loose_sr = (b_nBJets>=2 && b_mbb>80 && b_mbb<170 && b_mll<60);
        bool pass_loose_top = (b_nBJets>=2 && (b_mbb>140 || b_mbb<100) && b_mll<60);
        bool pass_loose_z = (b_nBJets>=2 && b_mbb>80 && b_mbb<220 && b_mll>60 && b_mll<120);
        fill_output = (pass_loose_sr || pass_loose_top || pass_loose_z);
    }

    // Fill
    if(fill_output)
    {
        output_tree()->Fill();
    }
    return;
}

//======================
} // namespace wwbb
