#
# package: WWbb2L2X
# author: daniel.joseph.antrim@CERN.CH
#         dantrim@uci.edu
# date: November 2018
#

atlas_subdir( wwbb2l2x )

set( extra_deps )
set( extra_libs )

atlas_depends_on_subdirs(
    PUBLIC
    PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces
    PhysicsAnalysis/AnalysisCommon/PMGTools
    PhysicsAnalysis/SUSYPhys/SUSYTools
    PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection
    PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools
    PhysicsAnalysis/ElectronPhotonID/EgammaAnalysisHelpers
    PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections
    Event/xAOD/xAODTruth
    Event/xAOD/xAODJet
    Event/xAOD/xAODEgamma
    Event/xAOD/xAODMuon
    Event/xAOD/xAODMissingET
    Event/xAOD/xAODBTagging
    PhysicsAnalysis/JetMissingEtID/JetSelectorTools
    Reconstruction/Jet/JetCPInterfaces
    Reconstruction/Jet/JetCalibTool
    PhysicsAnalysis/MCTruthClassifier
    PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
    PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces
    Trigger/TrigAnalysis/TrigDecisionTool
    Trigger/TrigConfiguration/TrigConfxAOD
    PhysicsAnalysis/AnalysisCommon/IsolationSelection
    Control/xAODRootAccess
    Event/xAOD/xAODCore
    Event/xAOD/xAODCutFlow
    Event/xAOD/xAODPrimitives
    Event/xAOD/xAODMetaData
    Control/AthToolSupport/AsgTools
    Tools/PathResolver
    PRIVATE
    ${extra_deps}
)

find_package( ROOT COMPONENTS Gpad Graf Graf3d Core Tree MathCore Hist Tree RIO )
find_package( lwtnn REQUIRED )

atlas_add_library( WWbb2L2XLib
    wwbb2l2x/*.h wwbb2l2x/utility/* src/*.cxx src/utility/*.cxx
    PUBLIC_HEADERS wwbb2l2x wwbb2l2x/utility/
    PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${LWTNN_INCLUDE_DIRS}
    LINK_LIBRARIES SUSYToolsLib AsgTools xAODCore xAODPrimitives xAODCutFlow
    ElectronEfficiencyCorrectionLib ElectronPhotonSelectorToolsLib PMGAnalysisInterfacesLib
    xAODRootAccess
    xAODTruth
    xAODJet
    JetCalibToolsLib
    JetCPInterfaces
    MCTruthClassifierLib
    PileupReweightingLib
    FTagAnalysisInterfacesLib
    MuonEfficiencyCorrectionsLib
    GoodRunsListsLib
    TrigDecisionToolLib
    TrigConfxAODLib
    IsolationSelectionLib
    PathResolver
    xAODMetaData
    PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${LWTNN_LIBRARIES}
)

set( extra_libs )

function(WWbbExec filename)
    set(execname)
    get_filename_component(execname ${filename} NAME_WE)
    atlas_add_executable( ${execname} "util/${execname}.cxx"
        INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
        LINK_LIBRARIES ${ROOT_LIBRARIES} WWbb2L2XLib ${extra_libs}
    )
endfunction(WWbbExec)

file(GLOB files "util/*.cxx")
foreach(file ${files})
    WWbbExec(${file})
endforeach()

atlas_install_data( data/* )
