#!/bin/env python

import os, sys, glob
import subprocess
import argparse

def make_condor_file(inputs, input_name, condor_name, exec_name, tar_name, args) :

    full_tar = tar_name
    file_tar = full_tar.strip().split("/")[-1]

    with open(condor_name, "w") as f :
        f.write('universe = vanilla\n')
        #f.write('Requirements = (GLIDEIN_Site != "MWT2") && (GLIDEIN_Site != "UChicago&& (HAS_CVMFS_atlas_cern_ch =?= True)\n')
        #f.write('Requirements = ((GLIDEIN_Site == "UCSD") || (GLIDEIN_Site == "UCI")) && (HAS_CVMFS_atlas_cern_ch =?= True)\n')
        #f.write('Requirements = (GLIDEIN_Site == "MWT2") && (HAS_CVMFS_atlas_cern_ch =?= True)\n')
        #f.write('Requirements = (GLIDEIN_Site == "UCSD") && (HAS_CVMFS_atlas_cern_ch =?= True)\n')
        f.write('Requirements = (GLIDEIN_Site != "UChicago") && (GLIDEIN_Site != "Michigan") && (GLIDEIN_Site != "MWT2") && (GLIDEIN_Site != "SPRACE") && (GLIDEIN_Site != "Clemson") && (GLIDEIN_Site != "FIU_HPCOSG_CE") && (GLIDEIN_Site != "OSG_US_GSU_ACORE") && (GLIDEIN_Site != "OSG_US_USF_SC") && (GLIDEIN_Site != "SU-ITS") && (HAS_CVMFS_atlas_cern_ch =?= True)\n')
        f.write('+local=true\n')
        f.write('+site_local=true\n')
        f.write('+uc=true\n')
        f.write('+sdsc=true\n')
        f.write('executable = %s\n' % exec_name)
        f.write('should_transfer_files = YES\n')
        if not args.split :
            f.write('transfer_input_files = %s %s\n' % (args.tar, input_name))
        else :
            f.write('transfer_input_files = %s\n' % full_tar)
        f.write('use_x509userproxy = True\n')
        f.write('notification = Never\n')

        log_base = args.name
        print "Submitting %s: %d" % (args.name, len(inputs))
        n_split = 0
        for q in inputs :
            if not args.split :
                inp = q.strip().split("/")[-1]
            else :
                inp = q
            f.write('\n')
            f.write('arguments = %s %d %s\n' % (inp, n_split, file_tar))
            f.write('output = log_wwbb_%s_%d.out\n' % (log_base, n_split))
            f.write('log = log_wwbb_%s_%d.log\n' % (log_base, n_split))
            f.write('error = log_wwbb_%s_%d.err\n' % (log_base, n_split))
            f.write('queue\n')

            n_split += 1

def make_executable(exec_name) :

    with open(exec_name, "w") as f :
        f.write("#!/bin/bash\n\n\n")
        f.write('echo "---------------- %s ----------------"\n' % exec_name)
        f.write('hostname\n')
        f.write('echo "start: `date`"\n')
        f.write('echo "input arguments:"\n')
        f.write('input=${1}\n')
        f.write('suffix=${2}\n')
        f.write('tarfile=${3}\n')
        f.write('echo " > input         : ${input}"\n')
        f.write('echo " > suffix        : ${suffix}"\n')
        f.write('echo " > tar           : ${tarfile}"\n')
        f.write('while (( "$#" )); do\n')
        f.write('      shift\n')
        f.write('done\n\n')
        f.write('echo "untarring ${tarfile}"\n')
        f.write('tar -xvf ${tarfile}\n')
        f.write('echo "current directory structure:"\n')
        f.write('ls -ltrh \n\n')
        f.write('export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n')
        f.write('source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\n')
        f.write('asetup AnalysisBase,21.2.55\n')
        f.write('source build/x86*/setup.sh\n')
        f.write('which run_wwbb\n')
        f.write('cmd="run_wwbb -i ${input} --uncerts --suffix ${suffix} -n 100"\n')
        f.write('echo "calling: ${cmd}"\n')
        f.write('$cmd\n')
        f.write('echo "DONE!"\n')
        f.write('echo "final directory structure:"\n')
        f.write('ls -ltrh\n')
        f.write('echo "finish: `date`"\n')

def launch_jobs(args) :

    if not os.path.isdir(args.outdir) :
        print "Making output directory: %s" % args.outdir
        os.mkdir(args.outdir)
    input_name = os.path.abspath(args.input)
    print "input_name = %s" % input_name
    #sys.exit()

    inputs = [input_name]
    if args.split :
        tmp = []
        with open(inputs[0], "r") as input_file :
            for line in input_file :
                line = line.strip()
                tmp.append(line)
        inputs = tmp

    tar_name = os.path.abspath(args.tar)

    os.chdir(args.outdir)

    condor_filename = "submit_wwbb_%s.condor" % args.name
    exec_name = "run_wwbb_%s.sh" % args.name
    make_condor_file(inputs, input_name, condor_filename, exec_name, tar_name, args)
    make_executable(exec_name)
    cmd = "condor_submit %s" % condor_filename
    subprocess.call(cmd, shell = True)

def main() :

    parser = argparse.ArgumentParser(description = "Launch WWbb xAOD Looper to condor batch")
    parser.add_argument("-i", "--input", required = True,
        help = "Input filelist"
    )
    parser.add_argument("--split", action = "store_true", default = False,
        help = "Run a single job per sub-file in the dataset"
    )
    parser.add_argument("-o", "--outdir", required = True,
        help = "Output directory to dump outputs"
    )
    parser.add_argument("-n", "--name", required = True,
        help = "Give the job a name"
    )
    parser.add_argument("-t", "--tar", required = True,
        help = "Provide path to the tar file to transfer to the job"
    )
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print "ERROR Did not find requested input file (=%s)" % args.input
        sys.exit()

    if not os.path.exists(args.tar) :
        print "ERROR Tar file (=%s) not found" % args.tar
        sys.exit()


    launch_jobs(args)


if __name__ == "__main__" :
    main()

