#!/bin/env python

# requirements: have access to CVMFS (to access xsec database)

from __future__ import print_function
import sys, os, glob
import argparse

def main() :

    parser = argparse.ArgumentParser(description = "Get the xsec x BR for a set of WWbb2l2x inputs")
    parser.add_argument("-i", "--input", nargs = "+", required = True,
        help = "Provide (a set of) inputs"
    )
    parser.add_argument("-v", "--verbose", action = "store_true", default = False,
        help = "Be loud about it"
    )
    args = parser.parse_args()

    import ROOT as r
    r.gROOT.SetBatch(True)

    xsec_br_db = {}
    xsec_db_filename = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt"
    with open(xsec_db_filename, "r") as db_file :
        for line in db_file :
            line = line.strip()
            if "dataset" in line : continue # skip first line
            fields = line.split()
            dsid = int(fields[0])
            xsec = float(fields[2])
            filt_eff = float(fields[3])
            kfact = float(fields[4])
            xsec_br_db[dsid] = [xsec, filt_eff, kfact]

    for input_dataset in args.input :
        full_path = os.path.abspath(input_dataset)
        dsid = full_path.strip().split("mc16_13TeV.")
        dsid = int(dsid[1].split(".")[0])
        xsec, filt, kf = xsec_br_db[dsid]
        if args.verbose :
            print("[{}] : {} {} {}".format(dsid, xsec, filt, kf))
        xsec_times_br = xsec * filt * kf
        print("{} {}".format(dsid, xsec_times_br))
        


if __name__ == "__main__" :
    main()
